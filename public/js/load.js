var loadState = {
    preload: function(){
        game.stage.backgroundColor = "#330939";
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' }); 
        loadingLabel.anchor.setTo(0.5, 0.5);
    
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);

        
		
		//MUSIC
        game.load.audio('BGM1', ['assets/bgm1.ogg', 'assets/bgm1.mp3']);
        game.load.audio('BGM2', ['assets/bgm2.ogg', 'assets/bgm2.mp3']);
        game.load.audio('BGM3', ['assets/bgm3.ogg', 'assets/bgm3.mp3']);
        game.load.audio('BGM4', ['assets/bgm4.ogg', 'assets/bgm4.mp3']);
		game.load.audio('bigpoint', ['assets/bigpoint.ogg', 'assets/bigpoint.mp3']);
		game.load.audio('Hitboss', ['assets/Hit_boss.ogg', 'assets/Hit_boss.mp3']);		
        game.load.audio('dead', ['assets/die.ogg', 'assets/die.mp3']);
        game.load.image('menulabel', 'assets/menulabel.png');
        game.load.image('gametitle', 'share/gametitle.png');
        game.load.image('gametitle1', 'share/gametitle_1.png');
        game.load.image('GAMEOVER1', 'share/OVER1.png');

    },
    create: function(){
        // Go to the menu state 
        game.state.start('menu');
    }
};