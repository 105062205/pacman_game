var menuState = {
    create: function(){

        // Display the name of the game
        // var titleLabel = game.add.sprite(game.width/2, 125, 'gametitle'); 
        var titleLabel = game.add.sprite(game.width/2, 125, 'gametitle1'); 
        titleLabel.anchor.setTo(0.5, 0.5); 

        // Explain how to start the game
        var menuLabel = game.add.sprite(game.width/2+15, game.height-160, 'menulabel'); 
        menuLabel.anchor.setTo(0.5, 0.5); 
        
        // Show the player's name at the center of the screen
        var playerLabel = game.add.text(game.width/2, game.height/2-40, theName,  { font: '40px Tahoma, Geneva, sans-serif', fill: '#F3E937', fontWeight: 'bold' }); 
        playerLabel.anchor.setTo(0.5, 0.5);
        var theName = 'anonymous';
        var Ref = firebase.database().ref('temp/');
        Ref.on('value', function(snapshot){
            if(snapshot.exists()) theName = snapshot.val().name;
            playerLabel.setText(theName);
        });
          
        //Enter the players name
        //Record it and the score in firebase
        var instructLabel = game.add.text(game.width/2, game.height-50, "Press N to enter your name",{ font: '20px "Lucida Sans Unicode", "Lucida Grande", sans-serif', fill: '#ffffff' ,fontWeight: 'bold'});
        instructLabel.anchor.setTo(0.5,0.5);
        var nameKey = game.input.keyboard.addKey(Phaser.Keyboard.N);
        nameKey.onDown.add(this.name, this);

        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start' 
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start1, this);
        var leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        leftKey.onDown.add(this.start2, this);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        downKey.onDown.add(this.start3, this);
        var rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        rightKey.onDown.add(this.start4, this);

    },
    name: function(){
        var name = prompt("Please enter your name", "name");
        var newitem = firebase.database().ref('temp/');
        newitem.set({
            name: name
        })
    },
    start1: function(){
        // Start the actual game
        game.state.add('Game1', PacmanGame1, true);
        game.state.start('Game1');
    },
    start2: function(){
        // Start the actual game
        game.state.add('Game2', PacmanGame2, true);
        game.state.start('Game2');
    },
    start3: function(){
        // Start the actual game
        game.state.add('Game3', PacmanGame3, true);
        game.state.start('Game3');
    },
    start4: function(){
        // Start the actual game
        game.state.add('Game4', PacmanGame4, true);
        game.state.start('Game4');
    }
};