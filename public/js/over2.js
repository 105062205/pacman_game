var overState2 = {
    create: function(){

        var titleLabel = game.add.sprite(game.width/2, 125, 'GAMEOVER1'); 
        titleLabel.scale.setTo(0.85,0.85);
        titleLabel.anchor.setTo(0.5, 0.5); 

        // var nameLabel = game.add.text(game.width/2, 200, 'Game Over', {font: '75px Arial', fill:'#ffffff'});
        // nameLabel.anchor.setTo(0.5, 0.5);
        var startLabel = game.add.text(game.width/2, game.height-100,' Press the up arrow key to retry', { font: '22.5px "Lucida Sans Unicode", "Lucida Grande", sans-serif', fill: '#965849' , fontWeight: 'bold'});
        startLabel.anchor.setTo(0.5, 0.5);    
        var quitLabel = game.add.text(game.width/2, game.height-70,' Press the down arrow key to quit', { font: '22.5px "Lucida Sans Unicode", "Lucida Grande", sans-serif', fill: '#965849' , fontWeight: 'bold'});
        quitLabel.anchor.setTo(0.5, 0.5); 

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.try, this);

        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        downKey.onDown.add(this.quit, this);

        this.createScoreBoard();
        var Ref = firebase.database().ref('second/').orderByChild("score").limitToLast(3);
        var i=0;
        Ref.on('value', function(snapshot){
            snapshot.forEach(function(childSnapshot) {
                    if(i==0){
                        text3.setText(childSnapshot.val().name + '  ' + childSnapshot.val().score);
                        i++;
                    }else if(i==1){
                        text2.setText(childSnapshot.val().name + '  ' + childSnapshot.val().score);
                        i++;
                    }else{
                        console.log(childSnapshot.val().score);
                        text1.setText(childSnapshot.val().name + '  ' + childSnapshot.val().score);
                        i=0;
                    }
            });
        });
    },
    createScoreBoard: function(){
        var style = {font: '25px "Arial Black", Gadget, sans-serif', fill: '#ffffff' }

        text1 = game.add.text(game.width/2-100, game.height/2, '', style);
        text1.anchor.setTo(0, 0.5);

        text2 = game.add.text(game.width/2-100, game.height/2+30, '', style);
        text2.anchor.setTo(0, 0.5);

        text3 = game.add.text(game.width/2-100, game.height/2+60, '', style);
        text3.anchor.setTo(0, 0.5);
    },
    try: function(){
        game.stage.backgroundColor = "#330939";
        game.state.start('menu');
        
    },
    quit: function(){
        var R= firebase.database().ref('temp/');
        R.set({
            name: 'Anonymous'
        })
        game.stage.backgroundColor = "#330939";
        game.state.start('menu');
    },
    
};

